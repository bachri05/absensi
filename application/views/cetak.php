 <div class="page-inner">
                <div class="page-title">
                    <h3>Cetak</h3>
                    <div class="page-breadcrumb">
                        <ol class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                         
                            <li class="active">Cetak</li>
                        </ol>
                    </div>
                </div>
                <div id="main-wrapper">
                    <div class="row">
                        <div class="invoice col-md-12">
                            <div class="panel panel-white">
                            	<div class="panel panel-heading">
                            	
                            	</div>
                                <div class="panel-body">
                                    <div class="row">
                                    	<div class="col-md-4 col-md-offset-4">
                                    		<h1><center><b>DAFTAR HADIR</b></center></h1>
                                    	</div>
                                    	<div class="col-md-3 text-right">
                                            <h1>&nbsp;</h1>
                                            <a href="<?php echo base_url('cetak/export') ?>" class="btn btn-default"><i class="fa fa-print"></i> Cetak</a>
                                        </div>
                                    	&nbsp;
                                    	<div class="row">
	                                        
	                                        <div class="col-sm-1 col-sm-offset-2">
	                                            <h6>SATKER</h6>
	                                        </div>
	                                        <div class="col-md-6">
	                                        	<h6>: PERENCANAAN DAN PENGAWASAN  JALAN NASIONAL PROVINSI JAWA TENGAH</h6>
	                                        </div>
	                                    </div>
                                        <div class="row">
	                                        
	                                        <div class="col-sm-1 col-sm-offset-2">
	                                            <h6>PPK</h6>
	                                        </div>
	                                        <div class="col-md-6">
	                                        	<h6>: PERENCANAAN DAN PENGAWASAN  JALAN NASIONAL PROVINSI JAWA TENGAH</h6>
	                                        </div>
	                                    </div>
	                                    <div class="row">
	                                        
	                                        <div class="col-sm-1 col-sm-offset-2">
	                                            <h6>KONSULTAN</h6>
	                                        </div>
	                                        <div class="col-md-6">
	                                        	<h6>: PT. PURNAJASA BIMAPRATAMA (JO) PT. GARIS PUTIH SEJAJAR</h6>
	                                        </div>
	                                    </div>
	                                    <div class="row">
	                                        
	                                        <div class="col-sm-1 col-sm-offset-2">
	                                            <h6>NO. KONTRAK</h6>
	                                        </div>
	                                        <div class="col-md-6">
	                                        	<h6>: 02/KTR/APBN/P2JN.JATENG/II/2017</h6>
	                                        </div>
	                                    </div>
	                                    <div class="row">
	                                        
	                                        <div class="col-sm-1 col-sm-offset-2">
	                                            <h6>TGL. KONTRAK</h6>
	                                        </div>
	                                        <div class="col-md-6">
	                                        	<h6>: 7 FEBRUARI 2017</h6>
	                                        </div>
	                                    </div>
	                                    <div class="col-md-12">
                                    		<h5><center>PAKET 02 – PENGAWASAN TEKNIS JALAN DAN JEMBATAN </center></h5>
                                    	</div>
                                    	<div class="col-md-12">
                                    		<h5 style="color: red;"><center>BATAS JABAR - TEGAL - SLAWI (OPTIONAL)</center></h5>
                                    	</div>
                                    	<div class="row">
	                                    	<div class="col-md-5 col-sm-offset-9">
	                                    		Periode : 1   s/d  31 Bulan Maret 2017
	                                    	</div>
	                                    </div>
	                                    <br/>
                                        <div class="col-md-12">
                                        	<div class="table-responsive">
                                            <table class="table table-bordered">
                                                <thead>
                                                    <tr>
                                                    	
														
														
                                                        <th rowspan="2" style="width: 5%;">No</th>
                                                        <th rowspan="2" style="width: 10%;">Nama</th>
                                                        <th rowspan="2" style="width: 10%">Jabatan</th>
                                                        <th colspan="30"><center>Tanggal</center></th>
                                                    </tr>
                                                    <tr>
                                                        <th style="width: 1%;">1</th>
                                                        <th style="width: 1%;">2</th>
                                                        <th style="width: 1%;">3</th>
                                                       	<th style="width: 1%;">4</th>
                                                        <th style="width: 1%;">5</th>
                                                        <th style="width: 1%;">6</th>
                                                        <th style="width: 1%;">7</th>
                                                        <th style="width: 1%;">8</th>
                                                        <th style="width: 1%;">9</th>
                                                        <th style="width: 1%;">10</th>
                                                        <th style="width: 1%;">11</th>
                                                        <th style="width: 1%;">12</th>
                                                        <th style="width: 1%;">13</th>
                                                        <th style="width: 1%;">14</th>
                                                        <th style="width: 1%;">15</th>
                                                        <th style="width: 1%;">16</th>
                                                        <th style="width: 1%;">17</th>
                                                        <th style="width: 1%;">18</th>
                                                        <th style="width: 1%;">19</th>
                                                        <th style="width: 1%;">20</th>
                                                        <th style="width: 1%;">21</th>
                                                        <th style="width: 1%;">22</th>
                                                        <th style="width: 1%;">23</th>
                                                        <th style="width: 1%;">24</th>
                                                        <th style="width: 1%;">25</th>
                                                        <th style="width: 1%;">26</th>
                                                        <th style="width: 1%;">27</th>
                                                        <th style="width: 1%;">28</th>
                                                        <th style="width: 1%;">29</th>
                                                        <th style="width: 1%;">30</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="33"><b>I. Tenaga Ahli</b></td>
                                                        
                                                    </tr>
                                                    <?php 
                                                    $no = 1;
                                                    foreach ($data as $value) {
                                                    	# code...
                                                   ?>
                                                    <tr>
                                                        <td><?php echo $no; ?></td>
                                                        <td><?php echo $value['username']; ?></td>
                                                        <td><?php echo $value['jabatan']; ?></td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                    </tr>
                                                <?php 
                                                $no++;
                                                } ?>
                                                   
                                                    <tr>
                                                        <td colspan="33"><b>II. Tenaga Pendukung</b></td>
                                                        
                                                    </tr>
                                                    <?php 
                                                    $no = 1;
                                                    foreach ($data as $value) {
                                                    	# code...
                                                   ?>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><?php echo $value['namatenaga'] ?></td>
                                                        <td><?php echo $value['jabatan'] ?></td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                        <td>1</td>
                                                    </tr>
                                                    <?php 
                                                    $no++;
                                                    } ?>
                                                    
                                                    
                                                </tbody>
                                            </table>


                                            </div>
                                            <br/>
                                             <div class="col-md-2">
                                            	Keterangan :<br/>
                                            	<table>
                                            	<tr>
                                            		<td>L</td><td>&nbsp;:</td><td>&nbsp;Libur</td>
                                            	</tr>
                                            	<tr>
                                            		<td>DL</td><td>&nbsp;:</td><td>&nbsp;Dinas Luar</td>
                                            	</tr>
                                            	<tr>
                                            		<td>A</td><td>&nbsp;:</td><td>&nbsp;Absen/Tidak Hadir</td>
                                            	</tr>
                                            	<tr>
                                            		<td>S</td><td>&nbsp;:</td><td>&nbsp;Sakit</td>
                                            	</tr>
                                            	<tr>
                                            		<td>C</td><td>&nbsp;:</td><td>&nbsp;Cuti</td>
                                            	</tr>
                                            	</table>
                                            </div>
                                            <div class="col-md-4">
                                            <table>	
                                            	<tr>
                                            		<td><center>SATUAN KERJA PERENCANAAN DAN PENGAWASAN</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>JALAN NASIONAL PROVINSI JAWA TENGAH</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>PPK PERENCANAAN DAN PENGAWASAN</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>JALAN NASIONAL PROVINSI JAWA TENGAH</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td>&nbsp;</td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>Ir. AGUNG HARI PRABOWO, MT</center></td>
                                            		
                                            	</tr>
                                            	<tr>
                                            		<td><center>NIP................................................</center></td>
                                            		
                                            	</tr>
                                            </table>
                                            </div>
                                            <div class="col-md-3">
                                            <table>	
                                            	<tr>
                                            		<td><center>PEJABAT PEMBUAT KOMITMEN							</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>...............................</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>&nbsp;</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>&nbsp;</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td>&nbsp;</td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>(NAMA PPK FISIK)</center></td>
                                            		
                                            	</tr>
                                            	<tr>
                                            		<td><center>NIP................................................</center></td>
                                            		
                                            	</tr>
                                            </table>
                                            </div>
                                            <div class="col-md-3">
                                            <table>	
                                            	<tr>
                                            		<td><center>KONSULTAN PENGAWASAN</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>PT...............................(KSO)</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>PT........................</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>&nbsp;</center></td>
                                            	</tr>
                                            	<tr>
                                            		<td>&nbsp;</td>
                                            	</tr>
                                            	<tr>
                                            		<td><center>(NAMA SITE ENGINEER)</center></td>
                                            		
                                            	</tr>
                                            	<tr>
                                            		<td><center>Site Engineer</center></td>
                                            		
                                            	</tr>
                                            </table>
                                            </div>
                                        </div>
                                       
                                        
                                    </div><!--row-->
                                </div>
                            </div>
                        </div>
                    </div><!-- Row -->
                </div><!-- Main Wrapper -->